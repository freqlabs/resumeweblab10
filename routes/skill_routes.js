var express = require('express');
var router = express.Router();
var skill_dal = require('../model/skill_dal');
var helpers = require('./helpers');

var logOrRender = helpers.logOrRenderFor('skill');
var logOrRedirectAll = helpers.logOrRedirectAllFor('skill');

function logOrRenderResult(res, view) {
  function mkParams(result) {
    return { result: result };
  }
  return logOrRender(res, view, mkParams);
}

function logOrRenderSkill(res, view) {
  function mkParams(result) {
    return { skill: result[0] };
  }
  return logOrRender(res, view, mkParams);
}

// View All skills
router.get('/all', function(req, res) {
  var callback = logOrRenderResult(res, 'ViewAll');
  skill_dal.getAll(callback);
});

// View the skill for the given id
router.get('/', function(req, res) {
  if (req.query.skill_id == null) {
    res.send('skill_id is null');
  } else {
    var callback = logOrRenderResult(res, 'ViewById');
    skill_dal.getById(req.query.skill_id, callback);
  }
});

// Return the add a new skill form
router.get('/add', function(req, res) {
  res.render('skill/skillAdd', {});
});

// Insert the skill for the given id
router.get('/insert', function(req, res) {
  if (req.query.skill_name == null) {
    res.send('Skill name must be provided.');
  } else if (req.query.description == null) {
    res.send('Skill description must be provided.');
  } else {
    var callback = logOrRedirectAll(res);
    skill_dal.insert(req.query, callback);
  }
});

router.get('/edit', function(req, res) {
  if (req.query.skill_id == null) {
    res.send('skill_id is null');
  } else {
    var callback = logOrRenderSkill(res, 'Update');
    skill_dal.edit(req.query.skill_id, callback);
  }
});

router.get('/update', function(req, res) {
  var callback = logOrRedirectAll(res);
  skill_dal.update(req.query, callback);
});

// Delete a skill for the given skill_id
router.get('/delete', function(req, res) {
  if (req.query.skill_id == null) {
    res.send('skill_id is null');
  } else {
    var callback = logOrRedirectAll(res);
    skill_dal.delete(req.query.skill_id, callback);
  }
});

module.exports = router;

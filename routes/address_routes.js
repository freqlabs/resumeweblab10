var express = require('express');
var router = express.Router();
var address_dal = require('../model/address_dal');
var helpers = require('./helpers');

var logOrRender = helpers.logOrRenderFor('address');
var logOrRedirectAll = helpers.logOrRedirectAllFor('address');

function logOrRenderResult(res, view) {
  function mkParams(result) {
    return { result: result };
  }
  return logOrRender(res, view, mkParams);
}

function logOrRenderAddress(res, view) {
  function mkParams(result) {
    return { address: result[0] };
  }
  return logOrRender(res, view, mkParams);
}

// View All addresses
router.get('/all', function(req, res) {
  var callback = logOrRenderResult(res, 'ViewAll');
  address_dal.getAll(callback);
});

// View the address for the given id
router.get('/', function(req, res) {
  if (req.query.address_id == null) {
    res.send('address_id is null');
  } else {
    var callback = logOrRenderResult(res, 'ViewById');
    address_dal.getById(req.query.address_id, callback);
  }
});

// Return the add a new address form
router.get('/add', function(req, res) {
  res.render('address/addressAdd', {});
});

// Insert the address for the given id
router.get('/insert', function(req, res) {
  if (req.query.street == null) {
    res.send('Street address must be provided.');
  } else if (req.query.zip_code == null) {
    res.send('Zip code must be provided.');
  } else {
    var callback = logOrRedirectAll(res);
    address_dal.insert(req.query, callback);
  }
});

router.get('/edit', function(req, res) {
  if (req.query.address_id == null) {
    res.send('address_id is null');
  } else {
    var callback = logOrRenderAddress(res, 'Update');
    address_dal.edit(req.query.address_id, callback);
  }
});

router.get('/update', function(req, res) {
  var callback = logOrRedirectAll(res);
  address_dal.update(req.query, callback);
});

// Delete an address for the given address_id
router.get('/delete', function(req, res) {
  if (req.query.address_id == null) {
    res.send('address_id is null');
  } else {
    var callback = logOrRedirectAll(res);
    address_dal.delete(req.query.address_id, callback);
  }
});

module.exports = router;

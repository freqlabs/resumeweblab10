function logOrAction(res, action) {
  return function(err, result) {
    if (err) {
      console.log(err);
      res.send(err);
    } else {
      action(err, result);
    }
  };
}

exports.logOrRenderFor = function(model) {
  return function(res, view, mkParams) {
    function action(err, result) {
      var viewName = model + '/' + model + view;
      res.render(viewName, mkParams(result));
    }
    return logOrAction(res, action);
  };
};

exports.logOrRedirectAllFor = function(model) {
  return function(res) {
    function action(err, result) {
      var path = '/' + model + '/all';
      res.redirect(302, path);
    }
    return logOrAction(res, action);
  };
};

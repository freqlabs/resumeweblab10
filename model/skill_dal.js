var mysql = require('mysql');
var db = require('./db_connection.js');

var logWrapper = require('./helpers').logWrapper;

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
  var query = 'SELECT * FROM skill';

  connection.query(query, function(err, result) {
    callback(err, result);
  });
};

exports.getById = function(skill_id, callback) {
  var query = 'SELECT * FROM skill WHERE skill_id = ?';
  var queryData = [skill_id];
  connection.query(query, queryData, logWrapper(callback));
};

exports.insert = function(params, callback) {
  var query = 'INSERT INTO skill (skill_name, description) VALUES ?';
  var queryData = [[[params.skill_name, params.description]]];
  connection.query(query, queryData, logWrapper(callback));
};

exports.delete = function(skill_id, callback) {
  var query = 'DELETE FROM skill WHERE skill_id = ?';
  var queryData = [skill_id];
  connection.query(query, queryData, logWrapper(callback));
};

exports.update = function(params, callback) {
  var query = 'UPDATE skill SET skill_name = ?, description = ? WHERE skill_id = ?';
  var queryData = [params.skill_name, params.description, params.skill_id];
  connection.query(query, queryData, logWrapper(callback));
};

exports.edit = exports.getById;

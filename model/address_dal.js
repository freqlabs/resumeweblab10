var mysql   = require('mysql');
var db  = require('./db_connection.js');

var logWrapper = require('./helpers').logWrapper;

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
  var query = 'SELECT * FROM address';
  connection.query(query, logWrapper(callback));
};

exports.getById = function(address_id, callback) {
  var query = 'SELECT * FROM address WHERE address_id = ?';
  var queryData = [address_id];
  connection.query(query, queryData, logWrapper(callback));
};

exports.insert = function(params, callback) {
  var query = 'INSERT INTO address (street, zip_code) VALUES ?';
  var queryData = [[[params.street, params.zip_code]]];
  connection.query(query, queryData, logWrapper(callback));
};

exports.delete = function(address_id, callback) {
  var query = 'DELETE FROM address WHERE address_id = ?';
  var queryData = [address_id];
  connection.query(query, queryData, logWrapper(callback));
};

exports.update = function(params, callback) {
  var query = 'UPDATE address SET street = ?, zip_code = ? WHERE address_id = ?';
  var queryData = [params.street, params.zip_code, params.address_id];
  connection.query(query, queryData, logWrapper(callback));
};

exports.edit = exports.getById;

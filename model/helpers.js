exports.logWrapper = function(callback) {
  return function(err, result) {
    if (err) {
      console.log(err);
    }

    callback(err, result);
  };
}

var mysql = require('mysql');
var db = require('./db_connection.js');

var logWrapper = require('./helpers').logWrapper;

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
  var query = 'SELECT * FROM account';
  connection.query(query, logWrapper(callback));
};

exports.getById = function(account_id, callback) {
  var query = 'SELECT * FROM account WHERE account_id = ?';
  var queryData = [account_id];
  connection.query(query, queryData, logWrapper(callback));
};

exports.insert = function(params, callback) {
  var query = 'INSERT INTO account (email, first_name, last_name) VALUES ?';
  var queryData = [[[params.email, params.first_name, params.last_name]]];
  connection.query(query, queryData, logWrapper(callback));
};

exports.delete = function(account_id, callback) {
  var query = 'DELETE FROM account WHERE account_id = ?';
  var queryData = [account_id];
  connection.query(query, queryData, logWrapper(callback));
};

exports.update = function(params, callback) {
  var query = 'UPDATE account SET email = ?, first_name = ?, last_name = ? WHERE account_id = ?';
  var queryData = [params.email, params.first_name, params.last_name, params.account_id];
  connection.query(query, queryData, logWrapper(callback));
};

exports.edit = exports.getById;
